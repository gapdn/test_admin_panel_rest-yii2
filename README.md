
ТЕСТОВОЕ ЗАДАНИЕ
----------------
    Реализовать на Yii2 следующий функционал:
    Сделать админ часть к условному новостному сайту. В админке должны быть доступны операции CRUD с новостными статьями.
    Авторизация. Доступ к админке должен осуществляться по логин/паролю. Всего два типа пользователей: исполнители и администраторы. Исполнители могут видеть, править, удалять исключительно свои статьи. Администратор как свои, так и всех остальных пользователей.
    REST API с авторизацией для манипуляции со статьями от имени пользователей.
    Дополнение к REST API: Сохранение принятых по API фото в "архив" (папку). Наименование папок по номеру новости принятого в параметрах (news_id). Полученные данные по API сохранить в БД.
    
---------------
Примеры запросов к API:
---------------
Создание записи       
curl "https://step.totem.dp.ua/api/post" -H "Authorization: Bearer <user_access_token>" -H "Accept: application/json" -X POST -F title="your_title" -F content="your_content" -F "filename=@/Path/to/image/image.jpg"

Доступные параметры: title, content, filename


Обновление записи     
curl "https://step.totem.dp.ua/api/post/<your_post_id>" -H "Authorization: Bearer <user_access_token>" -H "Accept: application/json" -X PUT -d title="your_title" -d content="your_content"

Доступные параметры: title, content


Удаление записи       
curl "https://step.totem.dp.ua/api/post/<your_post_id>" -H "Authorization: Bearer <user_access_token>" -H "Accept: application/json" -X DELETE


Получение записей     
curl "https://step.totem.dp.ua/api/post" -H "Authorization: Bearer <user_access_token>" -H "Accept: application/json" -X GET


Получение записи      
curl "https://step.totem.dp.ua/api/post/<your_post_id>" -H "Authorization: Bearer <user_access_token>" -H "Accept: application/json" -X GET