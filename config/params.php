<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'storagePath' => '/var/www/gap/artpix/web/uploads/',
    'storageUri' => 'http://step.totem.dp.ua/uploads/',
    'maxFileSize' => 1024 * 1024,
];
