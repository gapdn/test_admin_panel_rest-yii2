<?php

namespace app\controllers;

use Yii;
use app\models\News;
use app\models\NewsForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    'allow' => true,
                    'actions' => ['index', 'view', 'create', 'update', 'delete'],
                    'roles' => ['admin', 'author'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $roles = Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());
        $query = News::find();
        if (empty($roles['admin'])) {
             $query = News::find()->where('user_id = :user_id', [':user_id' => \Yii::$app->user->getId()]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsForm();
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->filename = UploadedFile::getInstance($model, 'filename');
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'News created!');
                return $this->redirect('/news');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $need_to_update = false;
        $model = NewsForm::getNewsModel($id);
        if (\Yii::$app->user->can('updatePost', ['news' => $model])){
            if ($post = Yii::$app->request->post('NewsForm')) {
                $model->title = $post['title'];
                $model->content = $post['content'];
                if (!empty($_FILES)) {
                    $model->filename = UploadedFile::getInstance($model, 'filename');
                    $need_to_update = true;
                }
                if ($model->update($this->findModel($id), $need_to_update)) {
                    Yii::$app->session->setFlash('success', 'News updated!');
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            Yii::$app->session->setFlash('warning', 'You can\'t update another\'s post.');
            return $this->redirect('/news/index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
                
        if (\Yii::$app->user->can('deletePost', ['news' => $model])) {
            $model->delete();
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('warning', 'You can\'t delete another\'s post.');
            return $this->redirect('/news/index');
        }
        
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
