<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\AccessControl;
use Yii;
use yii\web\Response;
use app\models\News;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class PostController extends ActiveController {
    
    public $modelClass = 'app\models\News';
    public $viewAction = 'https://step.totem.dp.ua/post';


    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        //\Yii::$app->user->enableSession = false;
        
        return parent::beforeAction($action);
    }
    
//    public function afterAction($action, $result) {
//        //Yii::$app->session->destroy();
//        parent::afterAction($action, $result);
//    }
    
    public function behaviors() {
        
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
        ];
        $behaviors['authenticator']['authMethods'] = [
            [
                'class' => \yii\filters\auth\QueryParamAuth::className(),
            ],
            [
                'class' => \yii\filters\auth\HttpBearerAuth::className(),
            ],
        ];
        $behaviors['contentNegotiator']['formats']['json'] = Response::FORMAT_HTML;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete'],
            'rules' => [
                [
                'allow' => true,
                'actions' => ['index', 'view', 'create', 'update', 'delete'],
                'roles' => ['admin', 'author'],
                ],
            ],
        ];
        
        
        return $behaviors;
    }
    
    public function actions() {
        
        $actions = parent::actions();

        // disable actions
        unset($actions['delete'], $actions['create']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];        
        
        return $actions;
    }
    
    public function prepareDataProvider() {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $roles = Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());
        
        if (empty($roles['admin'])) {
            $id = \Yii::$app->user->getId();
            $filter =  "user_id =  $id";   
        } else {
            $filter = '';
        }
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        $modelClass = $this->modelClass;
        $query = $modelClass::find();
        
        if (!empty($filter)) {
            $query->andWhere($filter);
        }

        return Yii::createObject([
                    'class' => ActiveDataProvider::className(),
                    'query' => $query,
                    'pagination' => [
                        'params' => $requestParams,
                    ],
                    'sort' => [
                        'params' => $requestParams,
                    ],
        ]);
    }

    public function actionCreate() {

        $model = new $this->modelClass();
        
        $params = \Yii::$app->getRequest()->getBodyParams();
        $params['user_id'] = Yii::$app->user->getId();
        $model->load($params, '');

        if ($model->save()) {
            if (!empty($_FILES)) {
                $filename = $_FILES['filename']['name'];
                $file = $_FILES['filename']['tmp_name'];
                $model->filename = $this->_uploadFile($file, $filename, $model->id);
                $model->save();
            }
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
    
    public function actionDelete($id) {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = News::findModel($id);
        
        
        if (!empty($model)) {
            $this->checkAccess('delete', $model);
            if ($model->delete() === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            }

            Yii::$app->getResponse()->setStatusCode(204, 'OK');

            return ['status' => 'OK','message' => 'NEWS DELETED'];
        } else {
            Yii::$app->getResponse()->setStatusCode(403);
            
            return ['status' => 'forbidden', 'message' => 'Failed to delete the object for unknown reason.'];
        }

    }
    
    public function checkAccess($action, $model = null, $params = array()) {        
        
        if ($action === 'update' || $action === 'delete') {
            
            if ($model->user_id !== \Yii::$app->user->getId())
                throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s articles that you\'ve created.', $action));
        }
        
        return true;
    }
    
    private function _uploadFile($file,$name, $id) {
        
        $postdata = fopen( $file, "r" );
        $filename = "$id/" . $name;
        $path = Yii::$app->params['storagePath'] . "$id/" . $name;
        $path = FileHelper::normalizePath($path);
        FileHelper::createDirectory(dirname($path));
        $fp = fopen( $path, "w" );

        while( $data = fread( $postdata, 1024 ) ) {
            fwrite( $fp, $data );
        }

        fclose( $fp );
        fclose( $postdata );

        return $filename;
    }

}
