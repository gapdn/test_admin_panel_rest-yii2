<?php

namespace app\models;

use yii\db\ActiveRecord;

class News extends ActiveRecord {
    
    public static function tableName() {
        return '{{%news}}';
    }
    
    public function rules() {
        
        return [
            [['title', 'content', 'user_id', 'filename'], 'safe', 'skipOnEmpty' => true, ],
            
        ];
    }
    
    public function attributeLabels() {
        return [
            'filename' => 'Image',
        ];
    }
    
    public static function findModel($id) {
        
        return static::findOne(['id' => $id]);
    }
    
}
