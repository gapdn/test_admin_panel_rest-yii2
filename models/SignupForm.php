<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SignupForm extends Model
{
    public $name;
    public $password;
   


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name', 'password'], 'required'],
            ['name', 'unique', 'targetClass' => User::class],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->name = $this->name;
        
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateAccessToken();

        $auth = Yii::$app->authManager;
        $authorRole = $auth->getRole('author');
        $result =  $user->save();
        $auth->assign($authorRole, $user->getId());
        
        return $result;

    }
}
