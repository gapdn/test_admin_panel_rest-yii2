<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
use app\models\News;

class NewsForm  extends Model {
    
    
    public $filename;
    public $title;
    public $content;
    public $user_id;
    public $id;

    public function rules() {
        
        return [
            [['title', 'content', 'user_id', 'filename'], 'safe'],
            [
                ['filename'], 
                'file', 
                'skipOnEmpty' => true, 
                'extensions' => ['png', 'jpg'],
                'maxSize' => \Yii::$app->params['maxFileSize'],
            ],
        ];
    }
    
    public function save() {
        if ($this->validate()) {
            $news = new News();
            $news->title = $this->title;
            $news->content = $this->content;
            $news->user_id = $this->user_id;          
            if ($news->save()) {
                $news->filename = Yii::$app->storage->saveUploadedFile($this->filename, $news->id);
                $result = $news->save(false);
                
                return $result;
            }
        }
        
        return false;
    }
    
    public function update(News $model, $need_to_update) {
        $model->title = $this->title;
        $model->content = $this->content;
        $model->user_id = $this->user_id;
        $model->filename = $need_to_update ? Yii::$app->storage->saveUploadedFile($this->filename, $model->id) : $this->filename;
        
        if ($model->save()) {
            return true;
        }
        return false;
    }
    
    public static function getNewsModel($id) {
        
        if (($news_model = News::findOne($id)) !== null) {
            $model = new NewsForm();
            $model->title = $news_model->title;
            $model->content = $news_model->content;
            $model->user_id = $news_model->user_id;
            $model->filename = $news_model->filename;
            $model->id = $news_model->id;
            
            return $model;
        }

        return false;
    }
}
