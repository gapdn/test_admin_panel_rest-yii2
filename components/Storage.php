<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class Storage extends Component {
    
    
    private $fileName;
    
    /**
     * Save given UploadedFile instance to disk
     * @param UploadedFile $file
     * @return string|null
     */
    public function saveUploadedFile(UploadedFile $file = null, $id) {
        
        if (empty($file)) {
            return '';
        }
        $path = $this->preparePath($file, $id);
        
        if ($path && $file->saveAs($path)) {
            return $this->fileName;
        }
    }
    
    /**
     * Prepare path to save uploaded file
     * @param UploadedFile $file
     * @return string|null
     */
    protected function preparePath(UploadedFile $file, $id) {
        
        $this->fileName = $this->getFileName($file, $id);
        
        $path  = $this->getStoragePath() . $this->fileName;
        
        $path = FileHelper::normalizePath($path);
        if (FileHelper::createDirectory(dirname($path))) {
            return $path;
        }
    }

    public function getFileName(UploadedFile $file, $id) {
                
        $name = "$id/" . $file->name;
        
        return $name;
        
    }
    
    /**
     * @return string
     */
    protected function getStoragePath() {
        
        return Yii::$app->params['storagePath'];
    }
    
    /**
     * 
     * @param string $fileName
     * @return string
     */
    public function getFile(string $fileName) {
        return Yii::$app->params['storageUri'] . $fileName;
    }

}
