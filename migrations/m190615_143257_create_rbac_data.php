<?php

use yii\db\Migration;
//use Yii;
//use app\models\User;

/**
 * Class m190615_143257_create_rbac_data
 */
class m190615_143257_create_rbac_data extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        
        // add the rule
        $rule = new app\rbac\AuthorRule;
        $auth->add($rule);
        $updateOwnNews = $auth->createPermission('updateOwnNews');
        $updateOwnNews->ruleName = $rule->name;
        $auth->add($updateOwnNews);
        
        // Define permissions
        $viewPostPermission = $auth->createPermission('viewPost');
        $auth->add($viewPostPermission);
        
        $deletePostPermission = $auth->createPermission('deletePost');
        $auth->add($deletePostPermission);
        
        $updatePostPermission = $auth->createPermission('updatePost');
        $auth->add($updatePostPermission);
        
        $createPostPermission = $auth->createPermission('cretePost');
        $auth->add($createPostPermission);
        
        $viewUserListPermission = $auth->createPermission('viewUserList');
        $auth->add($viewUserListPermission);
        
        $viewUserPermission = $auth->createPermission('viewUser');
        $auth->add($viewUserPermission);
        
        $deleteUserPermission = $auth->createPermission('deleteUser');
        $auth->add($deleteUserPermission);
        
        $updateUserPermission = $auth->createPermission('updateUser');
        $auth->add($updateUserPermission);
        
        // Define roles
        $authorRole = $auth->createRole('author');
        $auth->add($authorRole);
        
        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);
        
        // Define roles - permissions relations
        $auth->addChild($authorRole, $viewPostPermission);
        $auth->addChild($authorRole, $createPostPermission);
        
        $auth->addChild($adminRole, $authorRole);
        $auth->addChild($adminRole, $viewUserListPermission);
        $auth->addChild($adminRole, $viewUserPermission);
        $auth->addChild($adminRole, $deleteUserPermission);
        $auth->addChild($adminRole, $updateUserPermission);
        $auth->addChild($adminRole, $updatePostPermission);
        $auth->addChild($adminRole, $deletePostPermission);
        
        $auth->addChild($updateOwnNews, $updatePostPermission);
        $auth->addChild($updateOwnNews, $deletePostPermission);
        $auth->addChild($authorRole, $updateOwnNews);
        
        //$auth->assign($authorRole, 2);
        $auth->assign($adminRole, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m190615_143257_create_rbac_data cannot be reverted.\n";

        return false;
    }

}
